import java.util.ArrayList;
import java.util.List;

public class BusinessTasks {

	public String getTask(String[] arr, int n) {
		n--;
		List<String> list = new ArrayList<String>();
		for(String s : arr) {
			list.add(s);
		}
		
		int pos = 0;
		while(list.size() > 1) {
			pos = (pos + n) % list.size();
			list.remove(pos);
			if(pos == list.size()) {
				pos = 0;
			}
		}
		return list.get(0);
	}
}
